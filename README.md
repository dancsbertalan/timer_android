Olvass el!
==========

**Timer táviránító kliens Androidos eszközökre**
===============

Ennek a szoftvernek az a célja, hogy időzítva tudjuk a számítógépünket “kezelni”. Ez alatt az értendő, hogy kikapcsolni/lezárni/kijelentkezni/újraindítani és természetesen lealtatni a gépünket. 
Az utóbbi funkcióhoz a felhasználónak (némely) is szükség lesz egy parancs futtatására. (Csak egyszer). 
Tartalmazni fogja ez a szoftver a “streaming” funkciót. Ezt a funkciót csak azok a felhasználók tudják használni akik rendszergazdai joggal rendelkesznek és így is indítják a szoftvert.
Ha ezt bekapcsoljuk akkor telefonon keresztül (Android) képesek leszünk vezérelni a szoftvert így állítani, hogy mikor melyik műveletet hajtsa végre a számítógép.

Ez a repository ezt a telefonos alkalmazást tartalmazza.
A Windowsos kliens repositoryja itt található: https://bitbucket.org/dancsbertalan/timer_windows/overview

Ugyan azokon a nyelveken lesz elérhető mint a Windows-os kliens.

## **Rendszer követelmények** ##
Android 4.1.2 (Jelly Bean) (Minimum SDK 16)

## **Felhasznált erőforrások:** ##

-   A képek,iconok a Gimp GNU képszerkesztő programmal készültek - módosításokat is ennek a szoftvernek a segítségével csináltam meg.
	
-	A fejlesztői környezet az Android Studio https://developer.android.com/studio/index.html

-    A program ikonja innen való: http://simpleicon.com/power-symbol-6.html

-    .ico kiterjesztésre történő konvertálásban segített: http://convertico.com/

