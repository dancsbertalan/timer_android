package com.dancs.bertalan.timerremote.Classes.Managers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.dancs.bertalan.timerremote.Classes.Consts.OtherConsts;
import com.dancs.bertalan.timerremote.Classes.Consts.SharedPrefConsts;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Berci on 2017. 11. 01..
 */

public class SharedPrefManager {

    static SharedPreferences sharedPreferences;
    static SharedPreferences.Editor sharedPreferencesEditor;

    public static void initSharedPrefManager(Activity activity){
        sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    public static void putLastUsedIp(String value){
        sharedPreferencesEditor.putString(SharedPrefConsts.KEY_LAST_USED_IP_FIELD,value);
        sharedPreferencesEditor.commit();
        putNewIpToAllUsedIp(value);
    }

    public static String getLastUsedIp(){
//        String ip = sharedPreferences.getString(SharedPrefConsts.KEY_LAST_USED_IP_FIELD,"");
        return sharedPreferences.getString(SharedPrefConsts.KEY_LAST_USED_IP_FIELD,"");
    }

    private static String getAllUsedIpAsString(){
        return sharedPreferences.getString(SharedPrefConsts.KEY_ALL_USED_IP_FIELD,"");
    }

    public static List<String> getAllUsedIpAsList(){
        String ipsString = getAllUsedIpAsString();
        List<String> ips;
        if(ipsString == ""){
            ips = new ArrayList<String>();
        }
        else{
            String[] ipsStringArray = ipsString.split(SharedPrefConsts.ARRAY_DELIMETER);
            ips = Arrays.asList(ipsString.split(SharedPrefConsts.ARRAY_DELIMETER));
        }
        Log.d("AllUsedIp",ips.toString());
        return ips;
    }

    private static void putNewIpToAllUsedIp(String value){
        List<String> ips = getAllUsedIpAsList();
        if(ips.size() == 0){
            sharedPreferencesEditor.putString(SharedPrefConsts.KEY_ALL_USED_IP_FIELD,value);
            sharedPreferencesEditor.commit();
        }
        else if(!ips.contains(value)){
            sharedPreferencesEditor.putString(SharedPrefConsts.KEY_ALL_USED_IP_FIELD,getAllUsedIpAsString()+SharedPrefConsts.ARRAY_DELIMETER+value);
            sharedPreferencesEditor.commit();
        }
    }


}
