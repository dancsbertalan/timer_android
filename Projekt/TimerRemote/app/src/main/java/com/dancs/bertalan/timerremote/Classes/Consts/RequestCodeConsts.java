package com.dancs.bertalan.timerremote.Classes.Consts;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Berci on 2017. 09. 28..
 */

public class RequestCodeConsts {

    //region MŰVELET KÓDOK
    public static final int KEY_SHUTDOWN_ACTION_CODE = 602;
    public static final int KEY_LOCK_ACTION_CODE = 603;
    public static final int KEY_LOGOUT_ACTION_CODE = 604;
    public static final int KEY_REBOOT_ACTION_CODE = 605;
    public static final int KEY_SLEEP_ACTION_CODE = 606;
    public static final int KEY_HIBERNATE_ACTION_CODE = 619;

    public static final ArrayList<Integer> KEY_ACTION_CODES_ARRAY = new ArrayList<Integer>(){
        {
            add(KEY_SHUTDOWN_ACTION_CODE);
            add(KEY_LOCK_ACTION_CODE);
            add(KEY_LOGOUT_ACTION_CODE);
            add(KEY_REBOOT_ACTION_CODE);
            add(KEY_SLEEP_ACTION_CODE);
            add(KEY_HIBERNATE_ACTION_CODE);
        }
    };
    //endregion

    public static final int KEY_ACTUAL_ACTION_NO_CODE = 601;

    //region EREDMÉNY KÓDOK
    public static final int KEY_YOUR_SELECTED_ACTION_STARTING_IS_SUCCESS_CODE = 607;
    public static final int KEY_YOUR_SELECTED_ACTION_STARTING_IS_FAILED_CODE = 608;
    public static final int KEY_ACTUAL_ACTION_STOPPING_IS_SUCCESS_CODE = 609;
    public static final int KEY_ACTUAL_ACTION_STOPPING_IS_FAILED_CODE = 610;
    //endregion
}
