package com.dancs.bertalan.timerremote.Classes.Views;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.Toast;

import com.dancs.bertalan.timerremote.Classes.Consts.UpdateKeys;
import com.dancs.bertalan.timerremote.Classes.Managers.SharedPrefManager;
import com.dancs.bertalan.timerremote.Classes.Models.User;
import com.dancs.bertalan.timerremote.Classes.ViewModels.MainActivityViewModel;
import com.dancs.bertalan.timerremote.R;
import com.dancs.bertalan.timerremote.databinding.MainActivityBinding;

import java.util.HashMap;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity implements MainActivityViewModel.OnDataChanged {

    MainActivityBinding binding;
    public MainActivityBinding getBinding(){
        return binding;
    }
    MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main_activity);
        SharedPrefManager.initSharedPrefManager(this);
        binding = DataBindingUtil.setContentView(this,R.layout.main_activity);
        viewModel = new MainActivityViewModel(User.getInstance(this),this);
        binding.setViewModel(viewModel);
    }

    private void userActionViewsStyleChange(){
        viewModel.getModel().setActualActionStyle(binding.actualActionTextView);
        viewModel.getModel().setRemainingTimeStyle(binding.reaminingTimeTextView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.onResume();
    }

    @Override
    public void onUpdateUi(String updateKey, Object params) {

        switch (updateKey){
            case UpdateKeys.NO_ACTUAL_ACTION_UPDATE_KEY:
                Toast.makeText(this, getString(R.string.no_actual_action_message), Toast.LENGTH_SHORT).show();
                break;

            case UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY:
                Toast.makeText(this, getString(R.string.no_setted_remotable_exception_message), Toast.LENGTH_SHORT).show();
                break;

            case UpdateKeys.YOUR_SELECTED_ACTION_STARTING_IS_FAILED_UPDATE_KEY:
                Toast.makeText(this,getString(R.string.an_action_is_already_running),Toast.LENGTH_SHORT).show();
                break;

            case UpdateKeys.INVALID_TIME_FORMAT:
                Toast.makeText(this, getString(R.string.invalid_hour_or_minute_message), Toast.LENGTH_SHORT).show();
                break;

            case UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY:
                userActionViewsStyleChange();
                switch ((String) params){
                    case UpdateKeys.CHANGED_ACTUAL_ACTION_FROM_STOP_UPDATE_KEY:
                        break;
                    case UpdateKeys.CHANGED_ACTUAL_ACTION_FROM_SET_UPDATE_KEY:
                        Toast.makeText(this, getString(R.string.your_selected_action_is_started), Toast.LENGTH_SHORT).show();
                        break;
                    case UpdateKeys.CHANGED_ACTUAL_ACTION_FROM_UPDATE_UPDATE_KEY:
                        break;
                }
                break;

            case UpdateKeys.STOP_ACTUAL_ACTION_STOPPING_FAILED_UPDATE_KEY:
                Toast.makeText(this, getString(R.string.selected_action_isnt_stopped), Toast.LENGTH_SHORT).show();
                break;

            case UpdateKeys.STOP_ACTUAL_ACTION_STOPPING_SUCCESS_UPDATE_KEY:
                Toast.makeText(this, getString(R.string.selected_action_is_stopped), Toast.LENGTH_SHORT).show();
                break;
            case UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE:
                Toast.makeText(this, getString(R.string.remoteable_ip_is_not_available), Toast.LENGTH_SHORT).show();
                break;
            case UpdateKeys.NO_IP_WRITED_ON_IP_SETTER_VIEW:
                Toast.makeText(this, getString(R.string.please_write_and_ip_address_first_message), Toast.LENGTH_SHORT).show();
                break;

        }

        binding.setViewModel(viewModel);
    }
}
