package com.dancs.bertalan.timerremote.Classes.Models;

import android.content.Context;
import android.widget.Toast;

import com.dancs.bertalan.timerremote.Helpers;
import com.dancs.bertalan.timerremote.R;

/**
 * Created by Berci on 2017. 08. 26..
 */

public class RemainingTime {
    public RemainingTime(String hour,String minute){
        try{
            setHour(hour);
            setMinute(minute);
        }catch (NumberFormatException nfe){
            throw nfe;
        }
    }

    public int getHour() {
        return hour;
    }

    public void setHour(String hour) {
        try{
            if (Helpers.tryParseInt(hour)){
                this.hour = Integer.parseInt(hour);
            }
        } catch (NumberFormatException nfe){
            throw nfe;
        }
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        try{
            if (Helpers.tryParseInt(minute)){
                this.minute = Integer.parseInt(minute);
            }
        } catch (NumberFormatException nfe){
            throw nfe;
        }
    }

    public String getFullTime(){
        return Helpers.alwaysTwoDigitNumber(getHour())+":"+Helpers.alwaysTwoDigitNumber(getMinute());
    }

    private int hour;
    private int minute;
}
