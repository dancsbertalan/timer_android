package com.dancs.bertalan.timerremote.Classes.Consts;

/**
 * Created by Berci on 2017. 09. 28..
 */

public class RequestFieldConsts {
    public static final String KEY_HOUR_FIELD = "hour";
    public static final String KEY_MINUTE_FIELD = "minute";
    public static final String KEY_CODE_FIELD = "code";
    public static final String KEY_DATA_FIELD = "data";
}
