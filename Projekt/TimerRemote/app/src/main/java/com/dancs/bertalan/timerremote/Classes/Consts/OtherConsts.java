package com.dancs.bertalan.timerremote.Classes.Consts;

/**
 * Created by Berci on 2017. 10. 25..
 */

public class OtherConsts {

    public static final String DIALOG_MANAGER_VIEW_ID_KEY = "view_id";
    public static final String DIALOG_MANAGER_STYLE_ID_KEY = "style_id";
    public static final String DIALOG_MANAGER_THEME_ID_KEY = "theme_id";
    public static final String DIALOG_MANAGER_CTX_KEY = "ctx";

    public static final String DIALOG_MANAGER_DIALOG_KEY = "dialog";
    public static final String DIALOG_MANAGER_LISTENER_V_KEY = "listener";
    public static final String DIALOG_MANAGER_LAST_IP_FROM_TEXT_VIEW_KEY = "last_ip_from_text_view";


}
