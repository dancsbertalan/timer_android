package com.dancs.bertalan.timerremote.Interfaces.Listeners;

import javax.annotation.Nullable;

/**
 * Created by Berci on 2017. 09. 06..
 */

public interface IValueEventListener{
    void OnDataChanged(@Nullable Object object);
}
