package com.dancs.bertalan.timerremote.Classes.Managers;

import android.util.Log;

import com.dancs.bertalan.timerremote.Classes.Consts.RequestCodeConsts;
import com.dancs.bertalan.timerremote.Classes.Consts.RequestFieldConsts;
import com.dancs.bertalan.timerremote.Classes.Consts.UpdateKeys;
import com.dancs.bertalan.timerremote.Classes.Models.RemainingTime;
import com.dancs.bertalan.timerremote.Classes.Models.User;
import com.dancs.bertalan.timerremote.Helpers;
import com.dancs.bertalan.timerremote.Interfaces.Apis.TimerRemoteApiClient;
import com.dancs.bertalan.timerremote.Interfaces.Listeners.IValueEventListener;
import com.dancs.bertalan.timerremote.Interfaces.Managers.IRemoteManager;
import com.dancs.bertalan.timerremote.R;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Berci on 2017. 08. 28..
 *
 * Ez maga a remote szolgáltatás. (Szerver)
 *
 */

public class RemoteManager implements IRemoteManager {

    private RemoteManager ()
    {

    }

    public static RemoteManager getInstance(){
        if (instance == null){
            instance = new RemoteManager();
        }
        return instance;
    }

    /**
     * A szervertől lekérjük az aktuális műveletet.
     * @param listener - Az a listener amelynek tartalmaznia kell azt a hívást amellyel a UI-n frissítjük ha minden rendben ment
     */
    @Override
    public void getActualAction(final IValueEventListener listener, final User user) {
        if (user.getRemotableIp().equals("")){
            listener.OnDataChanged(UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY);
        }
        else{
            call = timerRemoteApiClient.retrofit_actual_action();
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.d("ResponseActualAction","Success");
                    JsonObject data = response.body().getAsJsonObject(RequestFieldConsts.KEY_DATA_FIELD);
                    int code = data.get(RequestFieldConsts.KEY_CODE_FIELD).getAsInt();
                    switch (code){
                        case RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE:
                            if (!RequestCodeConsts.KEY_ACTION_CODES_ARRAY.contains(code) && user.getActualActionCode() != 0){
                                user.setActualActionCode(code);
                                user.setActualAction(Helpers.getActualAcionStringByCode(code,user.getCtx()));
                                user.setRemainingTime(new RemainingTime("00","00"));
                                listener.OnDataChanged(null); //adatváltoztatás
                            }
                            listener.OnDataChanged(RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE);
                            break;

                        default:
                            user.setActualActionCode(code);
                            user.setActualAction(Helpers.getActualAcionStringByCode(code,user.getCtx()));
                            user.setRemainingTime(new RemainingTime(data.get(RequestFieldConsts.KEY_HOUR_FIELD).getAsString(),data.get(RequestFieldConsts.KEY_MINUTE_FIELD).getAsString()));
                            listener.OnDataChanged(null);
                            break;
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d("ResponseActualAction","Failed");
                    ///TODO: Gyári hiba lehetőséget lekezelése
                    listener.OnDataChanged(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE);
                }
            });
        }
    }

    /**
     * A szerver felé felszólunk hogy az alábbi paraméterekkel szeretnénk egy műveletet végrehajtatni.
     * @param listener                      - Az a listener amelynek tartalmaznia kell azt a hívást amellyel a UI-n frissítjük ha minden rendben ment
     * @param user                          - A felhasználó akinek az adatait váltosztatjuk
     * @param newActionCode                 - Az új művelet kódja (szerver paraméter is)
     * @param newActualAction               - Az új művelet neve  (csak lokális paraméter - szervernek nem megy ki)
     * @param remainingTime                 - Az új idő           (szerver paraméter is)
     */
    @Override
    public void postSetTimer(final IValueEventListener listener, final User user, final int newActionCode, final String newActualAction, final RemainingTime remainingTime) {
        //Szerver felé szólunk
        if (user.getRemotableIp().equals("")){
            listener.OnDataChanged(UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY);
        }
        else{
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(RequestFieldConsts.KEY_CODE_FIELD,newActionCode);
            jsonObject.addProperty(RequestFieldConsts.KEY_MINUTE_FIELD,remainingTime.getMinute());
            jsonObject.addProperty(RequestFieldConsts.KEY_HOUR_FIELD,remainingTime.getHour());

            call = timerRemoteApiClient.retrofit_set_timer(jsonObject);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.d("ResponseSetTimer","Success");
                    JsonObject data = response.body().getAsJsonObject(RequestFieldConsts.KEY_DATA_FIELD);
                    switch (data.get(RequestFieldConsts.KEY_CODE_FIELD).getAsInt()){
                        case RequestCodeConsts.KEY_YOUR_SELECTED_ACTION_STARTING_IS_SUCCESS_CODE:
                            user.setRemainingTime(remainingTime);
                            user.setActualAction(newActualAction);
                            user.setActualActionCode(newActionCode);
                            listener.OnDataChanged(null);
                            break;
                        case RequestCodeConsts.KEY_YOUR_SELECTED_ACTION_STARTING_IS_FAILED_CODE:
                            listener.OnDataChanged(RequestCodeConsts.KEY_YOUR_SELECTED_ACTION_STARTING_IS_FAILED_CODE);
                            break;
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d("ResponseSetTimer","Failed");
                    listener.OnDataChanged(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE);
                    ///TODO: Gyári hiba lehetőséget lekezelése
                }
            });
        }

    }

    /**
     * A szeerver felé felszólunk, hogy az aktuálisan végrehajtásra váró műveletet le szeretnénk állítani
     * @param listener - Az a listener amelynek tartalmaznia kell azt a hívást mellyel a UI-t frissítjük.
     * @param user     - A felhasználó akinek az adatatit a kérés alapján változtatjukk
     */
    @Override
    public void postStopActualAction(final IValueEventListener listener,final User user){
        if (user.getRemotableIp().equals("")){
            listener.OnDataChanged(UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY);
        }
        else {
            call = timerRemoteApiClient.retrofit_stop_actual_action();
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.d("ResponseStopActualActio", "Success");
                    JsonObject data = response.body().getAsJsonObject(RequestFieldConsts.KEY_DATA_FIELD);
                    int code = data.get(RequestFieldConsts.KEY_CODE_FIELD).getAsInt();
                    switch (code) {
                        case RequestCodeConsts.KEY_ACTUAL_ACTION_STOPPING_IS_SUCCESS_CODE:
                            user.setRemainingTime(new RemainingTime("00", "00"));
                            user.setActualAction(user.getCtx().getString(R.string.no_actual_action_message));
                            user.setActualActionCode(RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE);
                            listener.OnDataChanged(null);
                            listener.OnDataChanged(RequestCodeConsts.KEY_ACTUAL_ACTION_STOPPING_IS_SUCCESS_CODE);
                            break;
                        case RequestCodeConsts.KEY_ACTUAL_ACTION_STOPPING_IS_FAILED_CODE:
                            listener.OnDataChanged(RequestCodeConsts.KEY_ACTUAL_ACTION_STOPPING_IS_FAILED_CODE);
                            break;
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d("ResponseActualAction", "Failed");
                    listener.OnDataChanged(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE);
                    ///TODO: Gyári hiba lehetőséget lekezelése
                }
            });
        }
    }

    public void setTimerRemoteApiClient(String ip){
        timerRemoteApiClient = Helpers.getRetrofit(ip).create(TimerRemoteApiClient.class);
    }

    private static RemoteManager instance;
    private TimerRemoteApiClient timerRemoteApiClient;
    private Call<JsonObject> call;
//    private User user;
}
