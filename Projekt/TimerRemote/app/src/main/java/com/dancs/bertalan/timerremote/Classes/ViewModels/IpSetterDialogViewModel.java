package com.dancs.bertalan.timerremote.Classes.ViewModels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.dancs.bertalan.timerremote.Classes.Managers.DialogManager;
import com.dancs.bertalan.timerremote.Classes.Managers.SharedPrefManager;
import com.dancs.bertalan.timerremote.Interfaces.ViewModels.IViewModel;
import com.dancs.bertalan.timerremote.R;

import java.util.List;

/**
 * Created by Berci on 2017. 10. 25..
 */

public class IpSetterDialogViewModel extends BaseObservable implements IViewModel {

    Context ctx;
    DialogManager.IIpSetterDialogListener listener;
    String lastUsedIpFromTextView;

    public IpSetterDialogViewModel(Context ctx,DialogManager.IIpSetterDialogListener listener,String lastUsedIpFromTextView){
        this.ctx = ctx;
        this.listener = listener;
        setLastUsedIpFromTextView(lastUsedIpFromTextView);
    }

    public String getLastUsedIpFromTextView(){
        return lastUsedIpFromTextView;
    }

    private void setLastUsedIpFromTextView(String lastUsedIpFromTextView){
        this.lastUsedIpFromTextView = lastUsedIpFromTextView;
    }

    public void cancelClicked(View view){
//        Toast.makeText(ctx, "Canceled", Toast.LENGTH_SHORT).show();
        listener.onCancelButtonClicked();
    }

    public void setClicked(View view){
//        Toast.makeText(ctx, "Setted", Toast.LENGTH_SHORT).show();
        listener.onSetButtonClicked(null);
    }

    @Override
    public void updateUi() {

    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

}
