package com.dancs.bertalan.timerremote.Classes.Consts;

/**
 * Created by Berci on 2017. 11. 03..
 */

public class SharedPrefConsts {

    public static final String KEY_LAST_USED_IP_FIELD = "last_used_ip";
    public static final String KEY_ALL_USED_IP_FIELD = "all_used_ip_fied";

    public static final String ARRAY_DELIMETER =  ",";

}
