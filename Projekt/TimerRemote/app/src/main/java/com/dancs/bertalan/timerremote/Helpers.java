package com.dancs.bertalan.timerremote;

import android.content.Context;
import android.util.Log;

import com.dancs.bertalan.timerremote.Classes.Consts.RequestConsts;
import com.google.gson.Gson;

import java.text.NumberFormat;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.dancs.bertalan.timerremote.Classes.Consts.RequestCodeConsts;

/**
 * Created by Berci on 2017. 08. 26..
 */

public class Helpers {

    private static Retrofit retrofit;
    private static OkHttpClient httpClient = new OkHttpClient.Builder().build();

    public static boolean tryParseInt(String value){
        try {
            Integer.parseInt(value);
            return true;
        }
        catch (NumberFormatException nfe){
            throw nfe;
        }
    }

    public static String alwaysTwoDigitNumber(int number){
        String retval = Integer.toString(number);

        if(retval.length() < 2){
            retval = "0" + Integer.toString(number);
        }

        return retval;
    }

    public static Retrofit getRetrofit(String ip){

        if(retrofit == null){
            retrofit = new Retrofit.Builder().client(httpClient).baseUrl(RequestConsts.createBaseLinkWithIp(ip)).addConverterFactory(GsonConverterFactory.create()).build();
        }
        else if(!retrofit.baseUrl().host().equals(ip)){
            retrofit = new Retrofit.Builder().client(httpClient).baseUrl(RequestConsts.createBaseLinkWithIp(ip)).addConverterFactory(GsonConverterFactory.create()).build();
        }
        Log.d("RetroFitBaseUrlHost",retrofit.baseUrl().host());
        return retrofit;
    }

    public static String getActualAcionStringByCode(int code, Context ctx){
        String retVal = ctx.getString(R.string.no_actual_action_message);

        switch (code){
            case RequestCodeConsts.KEY_LOCK_ACTION_CODE:
                retVal = ctx.getString(R.string.lock_message_button);
                break;

            case RequestCodeConsts.KEY_SLEEP_ACTION_CODE:
                retVal = ctx.getString(R.string.sleep_message_button);
                break;

            case RequestCodeConsts.KEY_LOGOUT_ACTION_CODE:
                retVal = ctx.getString(R.string.logout_message_button);
                break;

            case RequestCodeConsts.KEY_SHUTDOWN_ACTION_CODE:
                retVal = ctx.getString(R.string.shutdown_message_button);
                break;

            case RequestCodeConsts.KEY_REBOOT_ACTION_CODE:
                retVal = ctx.getString(R.string.reboot_message_button);
                break;
            case RequestCodeConsts.KEY_HIBERNATE_ACTION_CODE:
                retVal = ctx.getString(R.string.hibernate_message_button);
                break;

        }
        return retVal;
    }
}
