package com.dancs.bertalan.timerremote.Classes.Managers;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.dancs.bertalan.timerremote.Classes.Consts.OtherConsts;
import com.dancs.bertalan.timerremote.Classes.ViewModels.IpSetterDialogViewModel;
import com.dancs.bertalan.timerremote.Interfaces.Listeners.IListener;
import com.dancs.bertalan.timerremote.R;
import com.dancs.bertalan.timerremote.databinding.IpSetterDialogBinding;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.Serializable;

/**
 * Created by Berci on 2017. 10. 25..
 *
 * A dialogok kezelésére szolgáló osztály.
 */

public class DialogManager extends DialogFragment {

//    int viewId;
    static Bundle args = new Bundle();

    /**
     * A ipSetterDialog példányosításához
     */
    public static DialogManager newInstance(int viewId, int styleId, int themeId, Context ctx, String lastIpFromTextView) {
        DialogManager f = new DialogManager();

        args.putInt(OtherConsts.DIALOG_MANAGER_VIEW_ID_KEY, viewId);
        args.putInt(OtherConsts.DIALOG_MANAGER_STYLE_ID_KEY,styleId);
        args.putInt(OtherConsts.DIALOG_MANAGER_THEME_ID_KEY,themeId);
        args.putString(OtherConsts.DIALOG_MANAGER_LAST_IP_FROM_TEXT_VIEW_KEY,lastIpFromTextView);

        f.setArguments(args);

        return f;
    }

    public void setListener(IListener listener){
        args.putSerializable(OtherConsts.DIALOG_MANAGER_LISTENER_V_KEY,listener);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(getArguments().getInt(OtherConsts.DIALOG_MANAGER_STYLE_ID_KEY),getArguments().getInt(OtherConsts.DIALOG_MANAGER_THEME_ID_KEY));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v;
        final Context ctx = getActivity();
        IListener baseListener = (IListener) getArguments().getSerializable(OtherConsts.DIALOG_MANAGER_LISTENER_V_KEY);
        switch (getArguments().getInt(OtherConsts.DIALOG_MANAGER_VIEW_ID_KEY)){
            case R.layout.ip_setter_dialog:
                v = inflater.inflate(R.layout.ip_setter_dialog,container,false);
                final IIpSetterDialogListener listener = (IIpSetterDialogListener) baseListener;

                final IpSetterDialogBinding binding = DataBindingUtil.bind(v);
                IpSetterDialogViewModel viewModel = new IpSetterDialogViewModel(ctx,
                        new IIpSetterDialogListener() {
                    @Override
                    public void onCancelButtonClicked() {
                        listener.onCancelButtonClicked();
                    }

                    @Override
                    public void onSetButtonClicked(@Nullable String newIp) {
                        listener.onSetButtonClicked(binding.setRemoteableIpAddressEditText.getText().toString());
                    }
                },getArguments().getString(OtherConsts.DIALOG_MANAGER_LAST_IP_FROM_TEXT_VIEW_KEY));
                binding.setRemoteableIpAddressEditText.setAdapter(new ArrayAdapter<String>(ctx,android.R.layout.simple_dropdown_item_1line,SharedPrefManager.getAllUsedIpAsList()));
                binding.setViewModel(viewModel);
                break;
            default:
                v = inflater.inflate(R.layout.default_dialog,container,false);
                break;
        }

        return v;

    }

    public interface IIpSetterDialogListener extends IListener{
        void onCancelButtonClicked();
        void onSetButtonClicked(@Nullable String newIp);
    }
}
