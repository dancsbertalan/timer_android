package com.dancs.bertalan.timerremote.Classes.Consts;

/**
 * Created by Berci on 2017. 10. 20..
 */

public class UpdateKeys {

    public static final String NO_SETTED_REMOTABLE_IP_UPDATE_KEY = "noSettedRemotableIp";
    public static final String NO_IP_WRITED_ON_IP_SETTER_VIEW = "noIpWritedOnIpSetterView";
    public static final String NO_ACTUAL_ACTION_UPDATE_KEY = "noActualAction";
    public static final String YOUR_SELECTED_ACTION_STARTING_IS_FAILED_UPDATE_KEY = "yourSelectedActionStartingIsFailed";
//    public static final String YOUR_SELECTED_ACTION_STARTING_IS_SUCCESSED_UPDATE_KEY = "yourSelectedActionStartingIsSuccess";
    public static final String INVALID_TIME_FORMAT = "invalidTimeFormat";
    public static final String CHANGED_REMOTABLE_IP_UPDATE_KEY = "changedRemotableIp";

    public static final String CHANGED_ACTUAL_ACTION_UPDATE_KEY = "changedActualAction";
    public static final String CHANGED_ACTUAL_ACTION_FROM_SET_UPDATE_KEY = "changedActualActionFromSet";
    public static final String CHANGED_ACTUAL_ACTION_FROM_UPDATE_UPDATE_KEY = "changedActualActionFromUpdate";
    public static final String CHANGED_ACTUAL_ACTION_FROM_STOP_UPDATE_KEY = "changedActualActionFromStop";

    public static final String STOP_ACTUAL_ACTION_STOPPING_SUCCESS_UPDATE_KEY = "stopActualActionStoppingSuccess";
    public static final String STOP_ACTUAL_ACTION_STOPPING_FAILED_UPDATE_KEY = "stopActualActionFailedSuccess";


    public static final String REMOTEABLE_IP_IS_NOT_AVAILABLE = "remoteAbleIpIsNotAvailable";

}
