package com.dancs.bertalan.timerremote.Classes.Consts;

/**
 * Created by Berci on 2017. 08. 28..
 */

public class RequestConsts {

    public static final String createBaseLinkWithIp(String ip){
        return "http://" + ip + ":" + Integer.toString(SERVER_PORT) + "/";
    }

    public static final int SERVER_PORT = 7171;

    //region REQUEST KULCSOK
    public static final String SET_TIMER_REQUEST_KEY = "set_timer";
    public static final String ACTUAL_ACTION_REQUEST_KEY = "actual_action";
    public static final String STOP_ACTION_REQUEST_KEY = "stop_actual_action";
    //endregion

}
