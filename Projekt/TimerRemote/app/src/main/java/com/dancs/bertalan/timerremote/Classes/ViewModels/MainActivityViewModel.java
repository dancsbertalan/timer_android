package com.dancs.bertalan.timerremote.Classes.ViewModels;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.dancs.bertalan.timerremote.Classes.Consts.OtherConsts;
import com.dancs.bertalan.timerremote.Classes.Consts.RequestCodeConsts;
import com.dancs.bertalan.timerremote.Classes.Consts.UpdateKeys;
import com.dancs.bertalan.timerremote.Classes.Managers.DialogManager;
import com.dancs.bertalan.timerremote.Classes.Managers.RemoteManager;
import com.dancs.bertalan.timerremote.Classes.Managers.SharedPrefManager;
import com.dancs.bertalan.timerremote.Classes.Models.RemainingTime;
import com.dancs.bertalan.timerremote.Classes.Models.User;
import com.dancs.bertalan.timerremote.Interfaces.Listeners.IValueEventListener;
import com.dancs.bertalan.timerremote.Interfaces.ViewModels.IViewModel;
import com.dancs.bertalan.timerremote.R;

/**
 * Created by Berci on 2017. 08. 25..
 */

public class MainActivityViewModel extends BaseObservable implements IViewModel {


    private User model;
    public User getModel() {
        return model;
    }

    private OnDataChanged listener;

    private String setableHour = "";
    private String setableMinute = "";
    private RemoteManager remoteManager;
    //TODO: A bemeneti szöveget filterelni.
//    @Bindable
//    public InputFilter[]  minuteEditTextFilter = new InputFilter[] {
//            new InputFilter() {
//                @Override
//                public CharSequence filter(CharSequence charSequence, int i, int i1, Spanned spanned, int i2, int i3) {
//
//                    String retval = "";
//                    for(int j = i; j < i1; j++){
//                        if(Character.isDigit(charSequence.charAt(j))){
//                            retval += charSequence.charAt(j);
//                        }
//                    }
//                    setableMinute = retval;
//                    return retval;
//                }
//            }
//    };
//
//    @Bindable
//    public InputFilter[]  hourEditTextFilter = new InputFilter[] {
//            new InputFilter() {
//                @Override
//                public CharSequence filter(CharSequence charSequence, int i, int i1, Spanned spanned, int i2, int i3) {
//
//                    String retval = "";
//                    for(int j = i; j < i1; j++){
//                        if(Character.isDigit(charSequence.charAt(j))){
//                            retval += charSequence.charAt(j);
//                        }
//                    }
//                    setableHour += retval;
//                    return retval;
//                }
//            }
//    };

    @Bindable
    public TextWatcher hourTextChangedListenerDUMMY = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            setableHour = charSequence.toString();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    public TextWatcher minuteTextChangedListenerDUMMY = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            setableMinute = charSequence.toString();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    public MainActivityViewModel(User model, final OnDataChanged listener){
        this.model = model;
        this.listener = listener;

        model.setRemotableIp(SharedPrefManager.getLastUsedIp(), new IValueEventListener() {

            //Hasonló alapon mint a dialog set click listenerje. Annyi változtatás van, hogy itt ha nem elérhető az utoljára használt cím akkor megnyitjuk azt.
            @Override
            public void OnDataChanged(@javax.annotation.Nullable Object object) {
                if(object instanceof Integer){

                    switch ((Integer) object){
                        case RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE:
                            listener.onUpdateUi(UpdateKeys.NO_ACTUAL_ACTION_UPDATE_KEY,null);
                            break;
                    }

                }else if(object instanceof String){
                    switch ((String) object){
                        case UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY:
                            listener.onUpdateUi(UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY,null);
                            break;
                        case UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE:
                            listener.onUpdateUi(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE,null);
                            break;
                        case UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY:
                            listener.onUpdateUi(UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY,UpdateKeys.CHANGED_ACTUAL_ACTION_FROM_UPDATE_UPDATE_KEY);
                            break;
                    }

                }
            }
        });

        remoteManager = RemoteManager.getInstance();
    }

    private void updateActualAction(){
        remoteManager.getActualAction(new IValueEventListener() {
            @Override
            public void OnDataChanged(Object object) {
                if (object instanceof Integer){
                    switch ((int) object){
                        case RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE:
//                                Toast.makeText(model.getCtx(), model.getCtx().getString(R.string.no_actual_action_message), Toast.LENGTH_SHORT).show();
                            listener.onUpdateUi(UpdateKeys.NO_ACTUAL_ACTION_UPDATE_KEY,null);
                            break;
                    }
                }
                else if(object instanceof String){
                    switch ((String) object){
                        case UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY:
                            listener.onUpdateUi(UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY,null);
                            break;
                        case UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE:
                            listener.onUpdateUi(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE,null);
                            break;
                    }
                }
                else if(object == null){
                    listener.onUpdateUi(UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY,UpdateKeys.CHANGED_ACTUAL_ACTION_FROM_UPDATE_UPDATE_KEY);
                }
            }
        },model);
    }

    public void updateActualAtionButtonClicked(View view){
        updateActualAction();
    }

    public void actionButtonCicked(View view){
        RemainingTime remainingTime;
        String actualAction = model.getCtx().getString(R.string.no_actual_action_message);//init
        int actualACtionCode = RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE; //init - Hiszen ha már a RemainingTime példányosítása megvan akkor már a másik kettő is el fog készülni
        try{
            remainingTime = new RemainingTime(setableHour,setableMinute);
            switch (view.getId()){
                case R.id.shutdown_button:
                    actualAction = model.getCtx().getString(R.string.shutdown_message_button);
                    actualACtionCode = RequestCodeConsts.KEY_SHUTDOWN_ACTION_CODE;
                    break;
                case R.id.reboot_button:
                    actualAction = model.getCtx().getString(R.string.reboot_message_button);
                    actualACtionCode = RequestCodeConsts.KEY_REBOOT_ACTION_CODE;
                    break;
                case R.id.sleep_button:
                    actualAction = model.getCtx().getString(R.string.sleep_message_button);
                    actualACtionCode = RequestCodeConsts.KEY_SLEEP_ACTION_CODE;
                    break;
                case R.id.lock_button:
                    actualAction = model.getCtx().getString(R.string.lock_message_button);
                    actualACtionCode = RequestCodeConsts.KEY_LOCK_ACTION_CODE;
                    break;
                case R.id.logout_button:
                    actualAction = model.getCtx().getString(R.string.logout_message_button);
                    actualACtionCode = RequestCodeConsts.KEY_LOGOUT_ACTION_CODE;
                    break;
                case R.id.hibernate_button:
                    actualAction = model.getCtx().getString(R.string.hibernate_message_button);
                    actualACtionCode = RequestCodeConsts.KEY_HIBERNATE_ACTION_CODE;
                    break;
            }
            remoteManager.postSetTimer(new IValueEventListener() {
//                String updateKey = UpdateKeys.YOUR_SELECTED_ACTION_STARTING_IS_SUCCESSED_UPDATE_KEY;
                @Override
                public void OnDataChanged(Object object) {
                    if (object instanceof Integer){
                        switch ((int) object){
                            case RequestCodeConsts.KEY_YOUR_SELECTED_ACTION_STARTING_IS_FAILED_CODE:
//                                Toast.makeText(model.getCtx(), model.getCtx().getString(R.string.an_action_is_already_running), Toast.LENGTH_SHORT).show();
//                                updateKey = UpdateKeys.YOUR_SELECTED_ACTION_STARTING_IS_FAILED_UPDATE_KEY;
                                listener.onUpdateUi(UpdateKeys.YOUR_SELECTED_ACTION_STARTING_IS_FAILED_UPDATE_KEY,null);
                                break;
                        }
                    }
                    else if (object instanceof String){
                        switch ((String) object){
                            case UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY:
                                listener.onUpdateUi(UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY,null);
                                break;
                            case UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE:
                                listener.onUpdateUi(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE,null);
                                break;
                        }
                    }
                    else if(object == null){
                        listener.onUpdateUi(UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY,UpdateKeys.CHANGED_ACTUAL_ACTION_FROM_SET_UPDATE_KEY);
                    }
                }
            },model,actualACtionCode,actualAction,remainingTime);
            //Szerver felé felküldjük az aktuális kérrést (művelet végrehajtást) amely az alábbi hiba lehetőségeket tartalmazhatja:
            //1. Nincs beállítva IP
            //2. A beállított IP nem elérhető
        }
        catch (NumberFormatException nfe){
//            Toast.makeText(model.getCtx(), model.getCtx().getString(R.string.invalid_hour_or_minute_message), Toast.LENGTH_SHORT).show();
            model.setActualAction(model.getCtx().getString(R.string.no_actual_action_message));
            model.setRemainingTime((new RemainingTime("0","0")));
            listener.onUpdateUi(UpdateKeys.INVALID_TIME_FORMAT,null);
        }
//        catch (RemotableIpIsNotAvailableException riinae){
//            Toast.makeText(model.getCtx(), riinae.getMessage(), Toast.LENGTH_SHORT).show();
//        }
    }

    public void stopActualActionButtonClicked(View view){
            remoteManager.postStopActualAction(new IValueEventListener() {
                @Override
                public void OnDataChanged(@javax.annotation.Nullable Object object) {
                    if (object instanceof Integer) {
                        switch ((int) object) {
                            case RequestCodeConsts.KEY_ACTUAL_ACTION_STOPPING_IS_SUCCESS_CODE:
                                listener.onUpdateUi(UpdateKeys.STOP_ACTUAL_ACTION_STOPPING_SUCCESS_UPDATE_KEY, null);
                                break;
                            case RequestCodeConsts.KEY_ACTUAL_ACTION_STOPPING_IS_FAILED_CODE:
                                listener.onUpdateUi(UpdateKeys.STOP_ACTUAL_ACTION_STOPPING_FAILED_UPDATE_KEY, null);
                                break;
                        }
                    } else if(object instanceof String){
                        switch ((String) object){
                            case UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY:
                                listener.onUpdateUi(UpdateKeys.NO_SETTED_REMOTABLE_IP_UPDATE_KEY,null);
                                break;
                            case UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE:
                                listener.onUpdateUi(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE,null);
                                break;
                        }
                    }
                    else if (object == null) {
                        listener.onUpdateUi(UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY, UpdateKeys.CHANGED_ACTUAL_ACTION_FROM_STOP_UPDATE_KEY);
                    }
                }
            }, model);

    }

    private String getLastUsedIpFromTextView(TextView textView){
        if(textView.getText().toString().equals(model.getCtx().getString(R.string.touch_here_to_set_remotable_ip_message))){
            return "";
        }
        return textView.getText().toString();
    }

    public void setIpAddressTextViewClicked(View view){
        FragmentManager fragmentManager = ((AppCompatActivity) model.getCtx()).getFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment previousFragment = fragmentManager.findFragmentByTag(OtherConsts.DIALOG_MANAGER_DIALOG_KEY);
        if(previousFragment != null){
            fragmentTransaction.remove(previousFragment);
        }
        fragmentTransaction.addToBackStack(null);

        final DialogManager ipSetterDialogManager = DialogManager.
                newInstance(R.layout.ip_setter_dialog, DialogFragment.STYLE_NORMAL, R.style.AppTheme_IpSetterDialogStyle, model.getCtx(),getLastUsedIpFromTextView(((TextView)view)));
        ipSetterDialogManager.setListener(new DialogManager.IIpSetterDialogListener() {
            @Override
            public void onCancelButtonClicked() {
                ipSetterDialogManager.dismiss();
            }

            @Override
            public void onSetButtonClicked(@Nullable final String newIp) {
                model.setRemotableIp(newIp, new IValueEventListener() {
                    //Hasonló alapon mint a viewModel konstruktorában. Annyi változtatás van, hogy itt nem zárhatjuk be a dialogot csak akkor ha sikerült elérnünk a címet.
                    @Override
                    public void OnDataChanged(@javax.annotation.Nullable Object object) {
                        if(object instanceof Integer){

                            switch ((Integer) object){
                                case RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE:
                                    listener.onUpdateUi(UpdateKeys.NO_ACTUAL_ACTION_UPDATE_KEY,null);
                                    ipSetterDialogManager.dismiss();
                                    SharedPrefManager.putLastUsedIp(newIp);
                                    break;
                            }

                        }else if(object instanceof String){
                            switch ((String) object){
                                case UpdateKeys.NO_IP_WRITED_ON_IP_SETTER_VIEW:
                                    listener.onUpdateUi(UpdateKeys.NO_IP_WRITED_ON_IP_SETTER_VIEW,null);
                                    break;
                                case UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE:
                                    listener.onUpdateUi(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE,null);
                                    break;
                                case UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY:
                                    listener.onUpdateUi(UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY,UpdateKeys.CHANGED_ACTUAL_ACTION_FROM_UPDATE_UPDATE_KEY);
                                    SharedPrefManager.putLastUsedIp(newIp);
                                    ipSetterDialogManager.dismiss();
                                    break;
                            }

                        }
                    }
                });
//                remoteManager.setTimerRemoteApiClient(newIp); //illegalurl
//                listener.onUpdateUi(UpdateKeys.CHANGED_REMOTABLE_IP_UPDATE_KEY,null);
            }
        });


        ipSetterDialogManager.show(fragmentManager,OtherConsts.DIALOG_MANAGER_DIALOG_KEY);
    }

    public String getRemotableIpUi(){
        if (model.getRemotableIp().equals("")){
            return model.getCtx().getString(R.string.touch_here_to_set_remotable_ip_message);
        }
        return model.getRemotableIp();
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {
        updateActualAction();
    }

    @Override
    public void updateUi() {

    }

    public interface OnDataChanged{
        void onUpdateUi(String updateKey,@Nullable Object param);
    }


}
