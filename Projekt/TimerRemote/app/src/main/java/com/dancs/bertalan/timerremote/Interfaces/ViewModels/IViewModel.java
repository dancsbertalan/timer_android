package com.dancs.bertalan.timerremote.Interfaces.ViewModels;

/**
 * Created by Berci on 2017. 08. 25..
 */

public interface IViewModel extends IUpdateUi {
    void onCreate();
    void onDestroy();
    void onPause();
    void onResume();
}
