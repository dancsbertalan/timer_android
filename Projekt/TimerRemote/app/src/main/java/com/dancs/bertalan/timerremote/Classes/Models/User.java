package com.dancs.bertalan.timerremote.Classes.Models;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Observable;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;

import com.dancs.bertalan.timerremote.Classes.Consts.RequestCodeConsts;
import com.dancs.bertalan.timerremote.Classes.Consts.RequestConsts;
import com.dancs.bertalan.timerremote.Classes.Consts.UpdateKeys;
import com.dancs.bertalan.timerremote.Classes.Managers.RemoteManager;
import com.dancs.bertalan.timerremote.Classes.Views.MainActivity;
import com.dancs.bertalan.timerremote.Interfaces.Listeners.IValueEventListener;
import com.dancs.bertalan.timerremote.R;

import javax.annotation.Nullable;

/**
 * Created by Berci on 2017. 08. 23..
 * A "User" osztály tartalmazza a hátralevő időt és ennek a jelenleg stílusát illetve a a jelenlegi műveletet is.
 * Továbbá azt az IP címet melyen keresztül elérjük a szervert.
 *
 */

public class User {

    //region KONSTRUKTOROK

    /**
     * Minden más inicializálás (felhasználó "elkérés") ezen keresztül történik.
     */
    private User() {

    }

    /**
     * Első inicializáláskor használandó konstruktor.
     */
    private User(Context ctx) {
        setCtx(ctx);
        setActualAction(getCtx().getString(R.string.no_actual_action_message));
        setRemainingTime(new RemainingTime("0","0"));
//        setRemotableIp("");
    }
    //endregion

    //region METÓDUSOK

    /**
     * Az első használatkor kell az inicializáláshoz.
     */
    public static User getInstance(Context ctx) {
        if (instance == null) {
            instance = new User(ctx);
        }
        return instance;
    }

    ///TODO: 1 - Kivétel kezeléssel le kezelni azt, hogy ha nem volt meg az első inicializálás.

    /**
     * Az első inicializálás után használható bárhol.
     */
    public static User getInstance() {
        if (instance == null) {
            instance = new User();
        }
        return instance;
    }
    //endregion

    //region SETTEREK ÉS GETTEREK
    public void setRemainingTime(RemainingTime remainingTime) {
        this.remainingTime = remainingTime;
    }

    public RemainingTime getRemainingTime() {
        return remainingTime;
    }

    public void setActualAction(String actualAction) {
        this.actualAction = actualAction;
    }

    public String getActualAction() {
        return actualAction;
    }

    public void setRemotableIp(String remotableIp, final IValueEventListener listener) {
        this.remotableIp = remotableIp;
        try {
            RemoteManager.getInstance().setTimerRemoteApiClient(remotableIp);
            RemoteManager.getInstance().getActualAction(new IValueEventListener() {
                @Override
                public void OnDataChanged(@Nullable Object object) {
                    //Miket kaphatok ? RemoteableIpIsNotAvailable, RemotableIpIsNotSetted, null, KEY_ACTUAL_ACTION_NO_CODEif (object instanceof Integer){
                    if (object instanceof Integer){
                        switch ((int) object){
                            case RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE:
//                                Toast.makeText(model.getCtx(), model.getCtx().getString(R.string.no_actual_action_message), Toast.LENGTH_SHORT).show();
                                listener.OnDataChanged(RequestCodeConsts.KEY_ACTUAL_ACTION_NO_CODE);
                                break;
                        }
                    }
                    else if(object instanceof String){
                        switch ((String) object){
                            case UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE:
                                instance.remotableIp = ""; ///TODO - 1 THIS-el
                                listener.OnDataChanged(UpdateKeys.REMOTEABLE_IP_IS_NOT_AVAILABLE);
                                break;
                        }
                    }
                    else if(object == null){
                        listener.OnDataChanged(UpdateKeys.CHANGED_ACTUAL_ACTION_UPDATE_KEY);
                    }
                }
            },this);
        } catch (IllegalArgumentException iae){
            instance.remotableIp = "";
            listener.OnDataChanged(UpdateKeys.NO_IP_WRITED_ON_IP_SETTER_VIEW);
        }
    }

    public String getRemotableIp() {
        return remotableIp;
    }

    public Context getCtx() {
        return ctx;
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }

    public int getActualActionCode() {
        return actualActionCode;
    }

    public void setActualActionCode(int actionCode) {
        this.actualActionCode = actionCode;
    }

    private void setTextViewStyleFromResource(TextView textView,int styleResId){
        if(Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(getCtx(),styleResId);
        }
        else{
            textView.setTextAppearance(styleResId);
        }
    }

    public void setActualActionStyle(TextView textView){
//        Log.d("SetActualActionStyleNow",Boolean.toString(getActualAction().equals(getCtx().getString(R.string.no_actual_action_message))));
        if (!getActualAction().equals(getCtx().getString(R.string.no_actual_action_message))){
            setTextViewStyleFromResource(textView,R.style.IsActualActionTimeStyle);
        }
        else{
            setTextViewStyleFromResource(textView,R.style.NoActualActionTimeStyle);
        }
    }

    public void setRemainingTimeStyle(TextView textView){
//        Log.d("SetReaminingTimStyleNow",Boolean.toString(getRemainingTime().getFullTime().equals("00:00")));
        if (!getRemainingTime().getFullTime().equals("00:00")){
            setTextViewStyleFromResource(textView,R.style.IsActualActionTimeStyle);
        }
        else{
            setTextViewStyleFromResource(textView,R.style.NoActualActionTimeStyle);
        }
    }
    //endregion

    //region MEZŐK
    private RemainingTime remainingTime;

    private String actualAction;
    private String remotableIp = "";
    private Context ctx;
    private int actualActionCode;

    private static User instance;

    //endregion
}
