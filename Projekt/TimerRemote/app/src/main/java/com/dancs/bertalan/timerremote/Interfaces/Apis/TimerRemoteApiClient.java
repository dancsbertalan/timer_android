package com.dancs.bertalan.timerremote.Interfaces.Apis;


import com.dancs.bertalan.timerremote.Classes.Consts.RequestConsts;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Berci on 2017. 09. 06..
 */

public interface TimerRemoteApiClient {

    @POST(RequestConsts.SET_TIMER_REQUEST_KEY)
    Call<JsonObject> retrofit_set_timer(@Body JsonObject requestBody);

    @GET(RequestConsts.ACTUAL_ACTION_REQUEST_KEY)
    Call<JsonObject> retrofit_actual_action();

    @POST(RequestConsts.STOP_ACTION_REQUEST_KEY)
    Call<JsonObject> retrofit_stop_actual_action();

}
