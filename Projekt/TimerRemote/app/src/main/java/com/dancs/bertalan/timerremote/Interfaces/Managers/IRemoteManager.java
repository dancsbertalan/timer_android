package com.dancs.bertalan.timerremote.Interfaces.Managers;

import com.dancs.bertalan.timerremote.Classes.Models.RemainingTime;
import com.dancs.bertalan.timerremote.Classes.Models.User;
import com.dancs.bertalan.timerremote.Interfaces.Listeners.IValueEventListener;

/**
 * Created by Berci on 2017. 08. 28..
 */

public interface IRemoteManager {

    void getActualAction(IValueEventListener listener,User user);
    void postSetTimer(IValueEventListener listener, User user,int newActionCode,String newActualAction, RemainingTime remainingTime);
    void postStopActualAction(IValueEventListener listener, User user);

}
